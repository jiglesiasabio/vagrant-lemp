echo Provisioning!

sudo apt-get update

# mySQL
# Force a blank root password for mysql

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'

sudo apt-get -y install mysql-server

sudo apt-get install -y python-software-properties
sudo add-apt-repository -y ppa:ondrej/php5

sudo apt-get update

sudo apt-get install -y nginx
sudo apt-get install -y php5-fpm php-pear

sudo apt-get install -y php5-common php5-mysqlnd php5-xmlrpc php5-curl php5-gd php5-cli php5-fpm php-pear php5-dev php5-imap php5-mcrypt

sudo pecl install xdebug

xdebugPath=$(find / -name 'xdebug.so' 2> /dev/null)

# echo "zend_extension=$xdebugPath" >> /etc/php5/fpm/php.ini
grep -q -F 'zend_extension='$xdebugPath /etc/php5/fpm/php.ini || echo 'zend_extension='$xdebugPath >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_enable=1" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_enable=1' /etc/php5/fpm/php.ini || echo 'xdebug.remote_enable=1' >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_autostart=1" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_autostart=1' /etc/php5/fpm/php.ini || echo 'xdebug.remote_autostart=1' >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_connect_back=on" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_connect_back=on' /etc/php5/fpm/php.ini || echo 'xdebug.remote_connect_back=on' >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_host=\"localhost\"" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_host="localhost"' /etc/php5/fpm/php.ini || echo 'xdebug.remote_host="localhost"' >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_port=9000" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_port=9000' /etc/php5/fpm/php.ini || echo 'xdebug.remote_port=9000' >> /etc/php5/fpm/php.ini
# echo "xdebug.remote_handler=\"dbgp\"" >> /etc/php5/fpm/php.ini
grep -q -F 'xdebug.remote_handler="dbgp"' /etc/php5/fpm/php.ini || echo 'xdebug.remote_handler="dbgp"' >> /etc/php5/fpm/php.ini

sudo cat > /etc/nginx/sites-available/default <<'EOF'
server {
    listen 80;

    server_name _;

    access_log /var/log/nginx/access.log;
    error_log /var/log/nginx/error.log;

    root /srv/web;
    #index app_dev.php;

    charset UTF-8;

    location / {
        try_files $uri $uri/ /app_dev.php?$query_string;
    }

    location ~ [^/]\.php(/|$) {
        #fastcgi_pass 127.0.0.1:9000;
        fastcgi_pass unix:/var/run/php5-fpm.sock;
        include fastcgi_params;
    }
}
EOF

sudo service nginx restart
sudo service php5-fpm restart
sudo /etc/init.d/mysql restart


# log.io web log reader http://logio.org/

# TODO CONFIGURE LOG HARVESTERS!!

#sudo apt-get install -y nodejs-legacy
#sudo apt-get install -y npm

#cd /home/vagrant
#sudo npm install -g log.io --user "ubuntu"
